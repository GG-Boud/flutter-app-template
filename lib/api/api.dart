import '/common/http/request.dart';

class Api {
  static Api? _instance;

  Api._(String serverUrl, int connectTimeout, String tokenKeyName) {
    _init(serverUrl, connectTimeout, tokenKeyName);
  }

  factory Api(
    String serverUrl, {
    int connectTimeout = 5,
    String tokenKeyName = "Auth-Token",
  }) {
    _instance ??= Api._(serverUrl, connectTimeout, tokenKeyName);
    return _instance!;
  }

  _init(String serverUrl, int connectTimeout, String tokenKeyName) {
    _tokenKeyName = tokenKeyName;
    // 初始化网络请求工具
    _request = Request(serverUrl, timeOut: connectTimeout);
  }

  late final Request _request;

  late final String _tokenKeyName;

  get tokenKeyName => _tokenKeyName;

  userList() {
    return _request.get("/user/userInfo");
  }

  authToken() {
    return _request.get("/auth/token");
  }
}
