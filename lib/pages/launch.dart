import 'dart:async';
import 'package:flutter/material.dart';
import '/common/utils/color_utils.dart';
import '/common/global.dart';

/// 启动页
class LaunchPage extends StatefulWidget {
  const LaunchPage({super.key});

  @override
  State<LaunchPage> createState() => _LaunchState();
}

class _LaunchState extends State<LaunchPage> {
  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 5), () => Global.router.goNamed("home"));
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: ColorUtils.getColorFromHex("#282e34"),
        ),
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "巧心视频",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              "- 记录美好生活 -",
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
