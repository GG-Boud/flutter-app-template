import 'package:flutter/material.dart';

class TestPage extends StatelessWidget {
  final String? id;
  const TestPage(this.id, {super.key});

  @override
  Widget build(BuildContext context) {
    print("id:$id");
    return const Column(
      children: <Widget>[Text("test页面")],
    );
  }
}
