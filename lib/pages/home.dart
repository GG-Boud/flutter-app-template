import 'package:flutter/material.dart';
import '/common/global.dart';
import '/pages/tabbar/index.dart';
import '/pages/tabbar/movie.dart';
import '/pages/tabbar/my.dart';
import '/pages/tabbar/today.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() => _TabbarState();
}

class _TabbarState extends State<HomePage> {
  int _index = 0;

  final List<Widget> _tabbars = const [
    IndexTabbar(),
    TodayTabbar(),
    MovieTabbar(),
    MyTabbar(),
  ];

  void _switchTabbar(int index) async {
    String? token = await Global.store.getData("authToken");
    Global.log.info(token);
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabbars[_index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        // fixedColor: Color(int.parse('0xff1E1E1E')),
        onTap: _switchTabbar,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "首页",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: "今日",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: "影视",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: "我的",
          ),
        ],
      ),
    );
  }
}
