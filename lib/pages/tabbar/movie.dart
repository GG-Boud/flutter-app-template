import 'package:flutter/material.dart';
import '/common/constant/assets_images.dart';

class MovieTabbar extends StatelessWidget {
  const MovieTabbar({super.key});

  Widget _buildView() {
    return Column(children: [
      Image.asset(
        AssetsImages.personImg,
        width: 200,
        height: 200,
      ),
      const SizedBox(height: 20),
      Image.network(
        "https://picsum.photos/id/1/200/300",
        width: 200,
        height: 200,
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movie"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "Movie Tab ssss  Ubuntu Q f F R r",
              style: TextStyle(
                fontFamily: "Ubuntu",
                fontWeight: FontWeight.bold,
              ),
            ),
            const Icon(Icons.apps),
            _buildView(),
          ],
        ),
      ),
    );
  }
}
