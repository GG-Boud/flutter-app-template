import 'package:flutter/material.dart';

class MyTabbar extends StatelessWidget {
  const MyTabbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My"),
        centerTitle: true,
      ),
      body: const Center(
        child: Text("My Tab"),
      ),
    );
  }
}
