import 'package:flutter/material.dart';
import '/common/global.dart';

class IndexTabbar extends StatelessWidget {
  const IndexTabbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Index",
          style: TextStyle(
            fontWeight: FontWeight.w400,
            //fontSize: 20,
            // fontFamily: "Ubuntu",
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(),
            TextButton(
                onPressed: () async {
                  // bool bo =
                  //     await Global.store.setData("authToken", "token1234");
                  // Global.log.info(bo);
                  // String? token = await Global.store.getData("authToken");
                  // Global.log.info(token);
                  Global.api.authToken();
                  Global.router.goNamed("test");
                },
                child: const Text("跳转到首页")),
          ],
        ),
      ),
    );
  }
}
