import 'package:flutter/material.dart';

class TodayTabbar extends StatelessWidget {
  const TodayTabbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Today"),
        centerTitle: true,
      ),
      body: const Center(
        child: Text("Today Tab"),
      ),
    );
  }
}
