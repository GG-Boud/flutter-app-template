import 'app_config.dart';
import '/common/logger/log.dart';
import '/common/store/easy_store.dart';
import '/common/router/router.dart';
import '/api/api.dart';

/// 全局对象
final class Global {
  // 日志实例
  static final Log log = Log();

  // 本地存储实例
  static const String _prefix = "";
  static final EasyStore store = EasyStore(_prefix);

  // Api接口实例
  static const String _serverUrl = AppConfig.apiServer;
  static const int _connectTimeout = 5;
  static const String _tokenKeyName = "Qiaoxin-Token";
  static final Api api = Api(
    _serverUrl,
    connectTimeout: _connectTimeout,
    tokenKeyName: _tokenKeyName,
  );

  // 路由器实例
  static final Router router = Router();
}
