import 'package:flutter/material.dart';

class ColorUtils {
  static Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    return Color(int.parse(hexColor, radix: 16) | 0xFF000000);
  }
}
