import 'package:logger/logger.dart';

final class Log {
  static final Log _log = Log._();

  Log._() {
    _init();
  }

  factory Log() {
    return _log;
  }

  late final Logger _logger;

  _init() {
    _logger = Logger(level: Level.debug);
  }

  info(
    dynamic message, {
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _logger.i(message, time: time, error: error, stackTrace: stackTrace);
  }

  debug(
    dynamic message, {
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _logger.d(message, time: time, error: error, stackTrace: stackTrace);
  }

  error(
    dynamic message, {
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _logger.e(message, time: time, error: error, stackTrace: stackTrace);
  }
}
