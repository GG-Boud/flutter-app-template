import 'dart:io';
import 'package:flutter_web_plugins/url_strategy.dart';
import '/common/http/http_overrides.dart';

/// 应用初始化操作
class Initialize {
  // 初始化
  static void init() {
    // web端使用hash路径模式
    usePathUrlStrategy();

    // 注：解决网络请求证书错误
    HttpOverrides.global = CustomHttpOverrides();
  }
}
