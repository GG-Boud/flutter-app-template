import 'package:flutter/material.dart';

import '/common/utils/color_utils.dart';

class AppConfig {
  // app基础信息配置
  static const String apiServer = "https://wx.qzgjwh.com/api";
  static const String appName = "qiaoxin-movie";
  static const String appVersion = "v1.0.0";
  static const String appAuthor = "liuxf";

  // 颜色配置
  static const String mainColor = "#161823";

  // app全局样式配置
  static final ThemeData appTheme = ThemeData(
    primaryColor: ColorUtils.getColorFromHex(mainColor),
    // 默认字体
    fontFamily: "AlibabaPuHuiTi",
    fontFamilyFallback: const ["Ubuntu", "AlibabaPuHuiTi"],
    // appbar
    appBarTheme: AppBarTheme(
      backgroundColor: ColorUtils.getColorFromHex(mainColor),
      foregroundColor: ColorUtils.getColorFromHex("#ffffff"),
      centerTitle: true,
    ),
    // 底部导航栏
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: ColorUtils.getColorFromHex(mainColor),
      selectedItemColor: ColorUtils.getColorFromHex("#ffffff"),
      unselectedItemColor:
          ColorUtils.getColorFromHex("#8a929a").withOpacity(0.5),
    ),
  );
}
