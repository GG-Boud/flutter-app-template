import 'package:shared_preferences/shared_preferences.dart';

// 简单的本地存储库 使用内置SharedPreferences对象
class EasyStore {
  static EasyStore? _instance;

  EasyStore._(String prefix) {
    _initStore(prefix);
  }

  factory EasyStore(String prefix) {
    _instance ??= EasyStore._(prefix);
    return _instance!;
  }

  _initStore(String prefix) async {
    SharedPreferences.setPrefix(prefix);
  }

  Future<SharedPreferences> _shared() async {
    SharedPreferences spf = await SharedPreferences.getInstance();
    return spf;
  }

  Future<bool> setData(String key, String value) async {
    SharedPreferences spf = await _shared();
    return spf.setString(key, value);
  }

  Future<String?> getData(String key) async {
    SharedPreferences spf = await _shared();
    return spf.getString(key);
  }

  Future<bool> removeData(String key) async {
    SharedPreferences spf = await _shared();
    return spf.remove(key);
  }
}
