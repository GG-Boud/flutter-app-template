import 'package:go_router/go_router.dart';
import '/common/global.dart';
import 'route_table.dart';

/// 路由器
class Router {
  static Router? _instance;

  Router._() {
    initRouteConfig();
  }

  factory Router() {
    _instance ??= Router._();
    return _instance!;
  }

  initRouteConfig() {
    _router = GoRouter(
      initialLocation: "/launch",
      onException: (context, state, router) =>
          {router.goNamed("404", extra: state.uri.toString())},
      routes: RouteTable.routeTable,
    );
  }

  late GoRouter _router;

  get router => _router;

  // 路由跳转
  goPath(String path) {
    Global.log.debug("跳转路由 ---> $path");
    _router.go(path);
  }

  // 路由跳转
  goNamed(String named) {
    String path = _router.namedLocation(named);
    Global.log.debug("跳转路由 ---> $path");
    _router.goNamed(named);
  }
}
