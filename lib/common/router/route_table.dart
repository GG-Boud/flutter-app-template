import 'package:go_router/go_router.dart';
import '/pages/home.dart';
import '/pages/test.dart';
import '/pages/launch.dart';
import '/pages/error/not_found.dart';

/// 定义路由表
final class RouteTable {
  static final List<RouteBase> routeTable = [
    GoRoute(
      path: "/launch",
      name: "launch",
      builder: (context, state) => const LaunchPage(),
    ),
    GoRoute(
      path: "/",
      redirect: (context, state) => context.namedLocation('home'),
    ),
    GoRoute(
      path: "/home",
      name: "home",
      builder: (context, state) => const HomePage(),
    ),
    GoRoute(
      path: "/test/:userId",
      name: "test",
      builder: (context, state) => TestPage(state.pathParameters['userId']),
    ),
    GoRoute(
      path: "/404",
      name: "404",
      builder: (context, state) => const NotFoundPage(),
    ),
  ];
}
