import 'package:flutter/material.dart';
import '/common/app_config.dart';
import '/common/global.dart';
import '/common/initialize.dart';

class App extends StatelessWidget {
  App({super.key}) {
    _initApp();
  }

  // 应用初始化
  _initApp() async {
    Initialize.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: AppConfig.appName,
      theme: AppConfig.appTheme,
      debugShowCheckedModeBanner: false,
      routerConfig: Global.router.router,
    );
  }
}
